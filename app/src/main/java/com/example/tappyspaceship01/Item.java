package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Item {

    // PROPERTIES:
    // Image
    // Hitbox
    private Bitmap image;
    private Bitmap item1;
    private Bitmap item2;
    private Bitmap item3;
    private Rect hitbox;

    private int xPosition;
    private int yPosition;

    public Item(Context context, int x, int y) {
        // 1. set up the initial position of the Enemy
        this.xPosition = x;
        this.yPosition = y;

        // 2. Set the default image - all enemies have same image
        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.alien_ship2);
        this.item1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.candy32);
        this.item2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.rainbow32);
        this.item3 = BitmapFactory.decodeResource(context.getResources(), R.drawable.poop32);

        // 3. Set the default hitbox - all enemies have same hitbox
        this.hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.image.getWidth(),
                this.yPosition + this.image.getHeight()
        );
    }

    // Getter and setters
    // Autogenerate this by doing Right Click --> Generate --> Getter&Setter

    public Bitmap getItem1() {
        return item1;
    }

    public Bitmap setItem1() {return  setItem1();}

    public Bitmap getItem2(){return item2;}

    public Bitmap setItem2() {return item2;}

    public Bitmap getItem3(){return item3;}

    public Bitmap setItem3(){return item3;}

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Rect getHitbox() {
        return hitbox;
    }

    public void setHitbox(Rect hitbox) {
        this.hitbox = hitbox;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public void updateHitbox(){
        this.hitbox.left = this.xPosition;
        this.hitbox.right = this.xPosition + this.image.getWidth();
        this.hitbox.top = this.yPosition;
        this.hitbox.bottom = this.yPosition + this.image.getHeight();
    }
}
