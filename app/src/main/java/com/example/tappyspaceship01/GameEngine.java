package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;
    final int min = 0;
    final int max = 500;
    int random = new Random().nextInt((max - min) + 1) + min;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;



    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    Player player;
    Item lane1;
    Item lane2;
    Item lane3;
    Item lane4;
    Item item1;
    Item item2;
    Item item3;

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // represent the TOP LEFT CORNER OF THE GRAPHIC
    int dinosaurXPosition;
    int dinosaurYPosition;
    Bitmap dinosaurImage;
    Rect dinosaurHitbox;

    //add candy
    Bitmap candyImage;
    int candyXPosition;
    int candyYPosition;
    Rect candyHitbox;


    // Add rainbow
    Bitmap rainbowImage;
    int rainbowXPosition;
    int rainbowYPosition;
    Rect rainbowHitbox;

    // Add garbage
    Bitmap garbageImage;
    int garbageXPosition;
    int garbageYPosition;
    Rect garbageHitbox;

    // ----------------------------
    // ## GAME STATS
    // ----------------------------
    int lives = 3;
    int score = 0;


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.printScreenInfo();
        int min = 50;
        int max = 200;

        lane1 = new Item(getContext(),0,screenHeight/2-250);
        lane2 = new Item(getContext(),0,screenHeight/2-125);
        lane3 = new Item(getContext(),0, screenHeight/2);
        lane4 = new Item(getContext(), 0 , screenHeight/2+125);

        int random = new Random().nextInt((max - min) + 1) + min;

        item1 = new Item(getContext(),0,screenHeight/2-random);
        item2 = new Item(getContext(),0,screenHeight/2-(random +125));
        item3 = new Item(getContext(),0,screenHeight/2+random);

        player = new Player(getContext(), 1000, 250);


    }



    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        //@TODO: Start the player at the left side of screen
    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {
//        if(this.fingerAction == "mousedown"){
//            player.setyPosition(player.getyPosition()-125);
//            player.updateHitbox();
//        }
//        else if(this.fingerAction == "mouseup"){
//            player.setyPosition(player.getyPosition()+10);
//            player.updateHitbox();
//        }

        this.item1.setxPosition(this.item1.getxPosition() + 10);
        this.item2.setxPosition(this.item2.getxPosition() + 5);
        this.item3.setxPosition(this.item3.getxPosition() + 20);
        item1.updateHitbox();
        item2.updateHitbox();
        item3.updateHitbox();


        //check the collision of items with dinosaur
        if (this.player.getHitbox().intersect(this.item1.getHitbox()) == true) {


            // increase the score colliding with candy
            score = score +1;
            item1 = new Item(getContext(),10,screenHeight/2-random);

        }
        if (this.player.getHitbox().intersect(this.item2.getHitbox()) == true) {



            // increase the score colliding with rainbow
            score = score +1;
            item2 = new Item(getContext(),10,screenHeight/2-(random+125));

        }
        if (this.player.getHitbox().intersect(this.item3.getHitbox()) == true) {



            // decrease the lives colliding with garbage
            lives = lives - 1;
            item3 = new Item(getContext(),10,screenHeight/2+random);

        }
    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);


            // DRAW THE PLAYER HITBOX
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);


            // draw player graphic on screen
            canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);
            // draw the player's hitbox
            canvas.drawRect(player.getHitbox(), paintbrush);

            paintbrush.setColor(Color.BLACK);


            //drawing lanes on the screen
            canvas.drawRect(lane1.getxPosition(),lane1.getyPosition(),
                    lane1.getxPosition()+1000,lane1.getyPosition()+10,paintbrush);
            canvas.drawRect(lane2.getxPosition(),lane2.getyPosition(),
                    lane2.getxPosition()+1000,lane2.getyPosition()+10,paintbrush);
            canvas.drawRect(lane3.getxPosition(),lane3.getyPosition(),
                    lane3.getxPosition()+1000,lane3.getyPosition()+10,paintbrush);
            canvas.drawRect(lane4.getxPosition(),lane4.getyPosition(),
                    lane4.getxPosition()+1000,lane4.getyPosition()+10,paintbrush);

            canvas.drawBitmap(item1.getItem1(), item1.getxPosition(), item1.getyPosition(), paintbrush);
            canvas.drawRect(item1.getHitbox(), paintbrush);
            canvas.drawBitmap(item2.getItem2(), item2.getxPosition(), item2.getyPosition(), paintbrush);
            canvas.drawRect(item2.getHitbox(), paintbrush);
            canvas.drawBitmap(item3.getItem3(), item3.getxPosition(), item3.getyPosition(), paintbrush);
            canvas.drawRect(item3.getHitbox(), paintbrush);


            // DRAW GAME STATS
            // -----------------------------
            paintbrush.setTextSize(30);
            paintbrush.setColor(Color.GREEN);
            canvas.drawText("Lives: " + lives,
                    800,
                    550,
                    paintbrush
            );

            paintbrush.setTextSize(30);
            paintbrush.setColor(Color.RED);
            canvas.drawText("Score: " + score,
                    600,
                    550,
                    paintbrush
            );

            //----------------
            this.holder.unlockCanvasAndPost(canvas);

        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    String fingerAction = "";

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            player.setyPosition(player.getyPosition()-100);
            if(player.getyPosition() > screenHeight/2 - 62.5) {
                player.setyPosition((player.getyPosition() - 125));
                player.updateHitbox();
            }
            else if(player.getyPosition()< screenHeight/2 - 62.5){
                player.setyPosition((player.getyPosition() + 125));
                player.updateHitbox();
            }

        }
        else if (userAction == MotionEvent.ACTION_UP) {
            player.setyPosition(player.getyPosition()+10);
            player.updateHitbox();
        }

        return true;
    }
}
